const express = require('express');
const router = express.Router();

const productBuyer = require('../controllers/productBuyer');
// const token = require('../middlewares/token');
// const sellerAuth = require('../middlewares/sellerAuth');
const readProductToken = require('../middlewares/readProductToken');

router.get('/:id',readProductToken.check,productBuyer.getProductDetail);
router.get('/',readProductToken.check,productBuyer.getProduct);

module.exports = router;