const express = require('express');
const router = express.Router();

const productSeller = require('../controllers/productSeller');
const ProductDataEncoder = require('../middlewares/uploadMultipleImage');
const userVerfication = require('../middlewares/userVerification');
const token = require('../middlewares/token');
const sellerAuth = require('../middlewares/sellerAuth');
const imageAuth = require('../middlewares/sellerProductImageAuth');
const productImage = require('../controllers/productGallery');
const productImageEncoder = require('../middlewares/uploadProductImage');

router.post('/', token.check, userVerfication.check,ProductDataEncoder,productSeller.addProduct);
router.put('/:id',token.check,sellerAuth.check, userVerfication.check,productSeller.updateProduct);
router.put('/image/:id', token.check,imageAuth.check, productImageEncoder, productImage.updateImage);
router.delete('/:id',token.check,sellerAuth.check, userVerfication.check,productSeller.deleteProduct);
router.get('/:id', token.check, sellerAuth.check,productSeller.getProductDetail);
router.get('/', token.check, productSeller.getProduct);
module.exports = router;