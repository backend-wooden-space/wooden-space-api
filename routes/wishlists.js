const express = require("express");
const router = express.Router();
const token = require('../middlewares/token');

const { createWishlist, getWishlists, deleteWishlistsById, deleteWishlistsByUserIdProductId } = require("../controllers/wishlist");

router.post("/productId/:productId", token.check, createWishlist);
router.get("/", token.check, getWishlists);
router.delete("/:wishlistId", token.check, deleteWishlistsById);
router.delete("/productId/:productId", token.check, deleteWishlistsByUserIdProductId);

module.exports = router;
