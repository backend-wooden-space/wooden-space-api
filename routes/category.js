const express = require('express');
const router = express.Router();

const category = require('../controllers/category');
const categoryAuth = require('../middlewares/categoryAuth');

router.post('/', categoryAuth.check, category.addCategory);
router.delete('/:id', categoryAuth.check, category.deleteCategory);
router.get('/',category.getAllCategory);

module.exports = router;