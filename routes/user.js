const express = require('express');
const router = express.Router();
const user = require('../controllers/user');
const upload = require('../middlewares/uploadSingleImage');
const token = require('../middlewares/token');
const emailClient = require('../controllers/emailClient');

router.put('/update_profile', upload, token.check, user.updateProfile);
router.get('/email_verification', token.check, emailClient.emailVerification);
router.get('/profile', token.check, user.getUserProfile);
router.get('/', user.getUser);
router.get('/:id', user.getUserDetail);
router.put('/', token.check, user.updateUser);
// router.put('/reset_password', )

module.exports = router;
