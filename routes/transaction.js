const express = require('express');
const router = express.Router();

const transaction = require('../controllers/transaction');
const userVerfication = require('../middlewares/userVerification');
const token = require('../middlewares/token');

router.post('/buyer', token.check, userVerfication.check, transaction.startTransaction);
router.put('/buyer/start_over/:id', token.check, userVerfication.check, transaction.startOverTransaction);
router.get('/buyer', token.check, userVerfication.check, transaction.buyerListTransaction);
router.put('/seller/:id', token.check, userVerfication.check, transaction.sellerUpdateTransaction);
router.get('/seller',token.check, userVerfication.check,transaction.sellerListTransactionByProduct);
router.put('/cancel/:id', token.check, userVerfication.check, transaction.cancelTransaction);
router.get('/seller/:id', token.check,transaction.sellerReadTransaction);
module.exports = router;