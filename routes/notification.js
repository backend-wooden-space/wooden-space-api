const express = require('express');
const router = express.Router();

const token = require('../middlewares/token');
const notification = require('../controllers/notification');

router.get('/',token.check,notification.getNotification);
router.put('/:id',token.check,notification.updateNotification);

module.exports = router;