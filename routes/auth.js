const express = require('express');
const router = express.Router();
const auth = require('../controllers/auth');
const emailClient = require('../controllers/emailClient');

router.post('/forgot_password', emailClient.forgotPassword)
router.put('/reset_password/:id', auth.resetPassword);
router.get('/email_verification', auth.emailVerification);

module.exports = router;
