const express = require('express');
const router = express.Router();
const user = require('./user');
const auth = require('./auth');
const wishlists = require('./wishlists');
const category = require('./category');
const productSeller = require('./productSeller');
const productBuyer = require('./productBuyer');
const transaction = require('./transaction');
const notification = require('./notification');

router.use('/user', user);
router.use('/auth', auth);
router.use('/category', category);
router.use('/seller/product', productSeller);
router.use('/buyer/product', productBuyer);
router.use('/transaction', transaction)
router.use('/wishlist', wishlists);
router.use('/notification', notification);

module.exports = router;
