const multer = require('multer');
// let uploadAvatar = multer()
let uploadAvatar = multer({
    fileFilter: (req,file,callback) => {
        if (file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg') {
            callback(null, true);
        } else {
            callback(null, false);
            callback(new Error('only png, jpg, and jped allowed to upload!'));
        }
    }
});

module.exports = uploadAvatar.single('avatar');