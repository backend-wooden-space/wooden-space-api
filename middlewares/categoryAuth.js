module.exports = {
    check: async (req,res,next) => {
        try {
            const key = req.headers.key; 
            if (key != 'adminws') {
                return res.status(401).json({
                    status:'fail',
                    message:'unauthorized',
                    data:null
                })
            }
            next();
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}