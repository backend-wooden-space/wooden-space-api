const multer = require('multer');
// let uploadAvatar = multer()
let uploadProductImages = multer({
    limits: { fileSize: 1 * 1024 * 1024 },
    fileFilter: (req,file,callback) => {
        if (file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg') {
            callback(null, true);
        } else {
            callback(null, false);
            callback(new Error('only png, jpg, and jped allowed to upload!'));
        }
    }
}).array('productImages', 4);

module.exports = uploadProductImages;