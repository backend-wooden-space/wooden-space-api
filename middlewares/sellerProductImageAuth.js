const {Product,User, ProductGallery} = require('../models');
module.exports = {
    check: async (req,res,next) => {
        try {
            const image_id = req.params.id;

            const productImage = await ProductGallery.findOne({
                where: {
                    id:image_id
                },
                include: {
                    model:Product,
                    as:'product',
                    include: {
                        model:User,
                        as:'owner',
                        attributes:['id','email']
                    }
                }
            });

            const user_id = productImage.product.owner.id;
            req.productImage = productImage;
            console.log(user_id, '==',req.user.id);
            if (user_id != req.user.id) {
                return res.status(401).json({
                    status:'fail',
                    message:'unauthorized',
                    data:null
                })
            }
            next();
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}