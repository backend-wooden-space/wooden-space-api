const {User} = require('../models');
module.exports = {
    check: async (req,res,next) => {
        try {
            const user = await User.findOne({
                where: {
                    id:req.user.id
                }
            })

            if (!user.isVerified) {
                return res.status(401).json({
                    status:"fail",
                    message:"User unverified",
                    data:null
                })
            } 
            next();
        } catch (err) {
            return res.status(500).json({
                status:"error",
                message:err.message,
                data:null
            })
        }
    }
}