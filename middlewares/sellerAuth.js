const {Product,User,ProductGallery,Category,Transaction,UserDetail} = require('../models');
module.exports = {
    check: async (req,res,next) => {
        try {
            const user_id = req.user.id;
    
            const product_id = req.params.id;
    
            const product = await Product.findOne({
                where:{
                    id:product_id
                },
                include: [{
                    model: User,
                    as:'owner',
                    attributes: ['id','email'],
                    include:[{
                        model: UserDetail,
                        as:'detail',
                        attributes: ['name','city','address','phone_number','avatar_url']
                    }]
                },{
                    model:Category,
                    as:'category',
                    attributes: ['name']
                },{
                    model:ProductGallery,
                    as:'product_images',
                    attributes:['url','file_id']
                }, {
                    model:Transaction,
                    as:'product_transactions'
                }]
            })
    
            req.product = product;
    
            if (!product) {
                return res.status(401).json({
                    status:'fail',
                    message:'product doesn\'t exist',
                    data:null
                })
            }
    
            if (product.owner.id != user_id) {
                return res.status(401).json({
                    status:'fail',
                    message:'user unauthorized',
                    data:null
                })
            }
            
            next();
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}