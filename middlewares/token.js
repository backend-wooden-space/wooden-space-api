const {User} = require('../models')
const jwt = require('jsonwebtoken')

module.exports = {
    check: async (req,res,next) => {
        try {
            // console.log(req);
            if (!req.headers.authorization) {
                return res.status(401).json({
                    status: false,
                    message: 'token is required!',
                    data: null
                });
            }
            const token = req.headers.authorization;
            const user = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);

            req.user = {
                id:user.id,
                email:user.email
            }
            next()
        } catch (err) {
            return res.status(500).json({
                status: 'error',
                message: err.message,
                data: null
            });
        }
    }
}