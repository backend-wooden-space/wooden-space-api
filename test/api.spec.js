const request = require("supertest");
const app = require("../app");
const truncate= require("./helpers/truncate");
const jwt = require('jsonwebtoken');
const {User} = require("../models");
require('dotenv').config();

const userMockUp = {
    username: "celespratama",
    email: "celespratama@gmail.com",
    password: "celes123",
}

const userMockUpLogin = {
    username: "celespratama",
    password: "celes123",
}

describe("User SignUp Endpoint Testing", () => {
    it('should return 201 if successfully created', async () => {
        try {
            const res = await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(userMockUp);
            
            expect(res.statusCode).toBe(201);
            expect(res.body).toHaveProperty("status", "success");
            expect(res.body).toHaveProperty("message", "user signed up");
            expect(res.body).toHaveProperty("data");
            expect(res.body.data).toHaveProperty("username", userMockUp.username);
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if user has already exist', async () => {
        try {
            const res = await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(userMockUp);
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "user has already exist");
            expect(res.body).toHaveProperty("data", null);
            await truncate.user();
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if user doesn\'t fill all required data', async () => {
        try {
            const invalidUser = {
                username: "celespratama"
            }
            const res = await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(invalidUser);
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message");
            expect(res.body).toHaveProperty("data", null);
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if invalid email', async () => {
        try {
            const invalidUser = {
                username: "celespratama",
                email: "celes",
                password: "123pass"
            }   
            const res = await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(invalidUser);
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message");
            expect(res.body).toHaveProperty("data", null);
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });
})

describe('User LogIn Endpoint Testing', () => {
    it('should return 200 if user successfully login', async () => {
        try {
            await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(userMockUp);
            
            const res = await request(app)
                .post('/api/v1/user/auth/login')
                .send(userMockUpLogin);
            
            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty("status", "success");
            expect(res.body).toHaveProperty("message", "user logged in");
            expect(res.body).toHaveProperty("data");
            expect(res.body.data).toHaveProperty("code");
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if username not found', async () => {
        try {
            const invalidUserLogin = {
                username: "celespetrus",
                password: "celes123"
            }

            const res = await request(app)
                .post('/api/v1/user/auth/login')
                .send(invalidUserLogin);
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "user doesn\'t exist");
            expect(res.body).toHaveProperty("data",null);
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if password invalid', async () => {
        try {
            const invalidUserLogin = {
                username: "celespratama",
                password: "password123"
            }

            const res = await request(app)
                .post('/api/v1/user/auth/login')
                .send(invalidUserLogin);
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "wrong password");
            expect(res.body).toHaveProperty("data",null);
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if user doesn\'t fill all required data', async () => {
        try {
            const invalidUserLogin = {
                username: "celespratama"
            }
            const res = await request(app)
                .post('/api/v1/user/auth/login')
                .send(invalidUserLogin);
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message");
            expect(res.body).toHaveProperty("data", null);
            await truncate.user();
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });
})

describe('User Email Verification Endpoint Testing', () => {
    it('should return 200 unless no authorization provided', async () => {
        try {
            await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(userMockUp);
            const resLogIn = await request(app)
                .post('/api/v1/user/auth/login')
                .send(userMockUpLogin);

            const header = {'Authorization': resLogIn.data.code, 'Content-Type': 'application/json'};

            const res = await request(app)
                .get('/api/v1/user/auth/email-verif')
                .set(header);
            
            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty("status", "success");
            expect(res.body).toHaveProperty("message", "email verification sent");
            expect(res.body).toHaveProperty("data");
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if no token header provided', async () => {
        try {
            const res = await request(app)
                .get('/api/v1/user/auth/email-verif');
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "invalid grant");
            expect(res.body).toHaveProperty("data");
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if no token header invalid', async () => {
        try {
            const header = {'Authorization': 'abcDEF123', 'Content-Type': 'application/json'};

            const res = await request(app)
                .get('/api/v1/user/auth/email-verif')
                .set(header);
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "invalid grant");
            expect(res.body).toHaveProperty("data");
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 200 if verified', async () => {
        try {
            const user = await User.findOne({
                where: {
                    username:userMockUp.username
                }
            })

            const jwtPayload = {
                id:user.id,
                username:user.username,
                email:user.email,
                goal:'email verifying'
            }

            const jwtToken = jwt.sign(jwtPayload, process.env.JWT_SECRET);

            const res = await request(app)
                .get('/api/v1/user/auth/email-verif')
                .query({code:jwtToken});
            
            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty("status", "success");
            expect(res.body).toHaveProperty("message", "email verified");
            expect(res.body).toHaveProperty("data");
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if invalid verification token', async () => {
        try {
            const user = await User.findOne({
                where: {
                    username:userMockUp.username
                }
            })

            const jwtPayload = {
                id:user.id,
                username:user.username,
                email:user.email,
                goal:'email invalid'
            }

            const jwtToken = jwt.sign(jwtPayload, process.env.JWT_SECRET);

            const res = await request(app)
                .get('/api/v1/user/auth/email-verif')
                .query({code:jwtToken});
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "invalid verification token");
            expect(res.body).toHaveProperty("data");
            await truncate.user();
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });
})

describe('User Forgot Password Endpoint Testing', () => {
    it('should return 200 if email exist', async () => {
        try {
            await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(userMockUp);
                
            const res = await request(app)
                .post('/api/v1/user/auth/forgot-password')
                .send({email:userMockUp.email});
            
            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty("status", "success");
            expect(res.body).toHaveProperty("message", "password reset link sent");
            expect(res.body).toHaveProperty("data");
            await truncate.user();
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if email doesn\'t exist', async () => {
        try {
            const res = await request(app)
                .post('/api/v1/user/auth/forgot-password')
                .send({email:userMockUp.email});
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "email doesn\'t exist");
            expect(res.body).toHaveProperty("data");
            await truncate.user();
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });
})

describe('User Reset Password Endpoint Testing', () => {
    it('should return 200 if reset password link is valid', async () => {
        try {
            await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(userMockUp);

            const user = await User.findOne({
                where: {
                    username:userMockUp.username
                }
            })

            const jwtPayload  = {
                id:user.id,
                email:user.email
            }

            const jwtToken = jwt.sign(jwtPayload, process.env.JWT_SECRET + user.password);

            const res = await request(app)
                .get('/api/v1/user/auth/reset-password')
                .query({code:jwtToken})
            
            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty("status", "success");
            expect(res.body).toHaveProperty("message", "access granted");
            expect(res.body).toHaveProperty("data");
            await truncate.user();
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if reset password link is invalid', async () => {
        try {
            const res = await request(app)
                .get('/api/v1/user/auth/reset-password')
                .query({code:'abcDEF123'})
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "invalid grant");
            expect(res.body).toHaveProperty("data");
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 200 if succesfully reset password', async () => {
        try {
            await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(userMockUp);

            const user = await User.findOne({
                where: {
                    username:userMockUp.username
                }
            })

            const jwtPayload  = {
                id:user.id,
                email:user.email
            }

            const jwtToken = jwt.sign(jwtPayload, process.env.JWT_SECRET + user.password);

            const resetPasswordBody = {
                newPassword: "password123",
                confirmPassword: "password123"
            }

            const res = await request(app)
                .post('/api/v1/user/auth/reset-password')
                .send(resetPasswordBody)
                .query({code:jwtToken})
            
            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty("status", "success");
            expect(res.body).toHaveProperty("message", "password changed");
            expect(res.body).toHaveProperty("data");
            await truncate.user();
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if reset password provide invalid token', async () => {
        try {
            const resetPasswordBody = {
                newPassword: "password123",
                confirmPassword: "password123"
            }

            const res = await request(app)
                .post('/api/v1/user/auth/reset-password')
                .send(resetPasswordBody)
                .query({code:'abcDEF123'})
            
            expect(res.statusCode).toBe(400);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "invalid grant");
            expect(res.body).toHaveProperty("data");
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });
})

describe('Update User Profile Endpoint Testing', () => {
    const testAvatar = `${__dirname}/assets/avatar1.jpg`;
    const userUpdateMockUp = {
        name: "Petrus Caelestinus",
        city: "Sleman",
        address: "Sanggrahan RT 006/RW 016",
        phone_number: "088888888888",
        avatar_url: testAvatar
    }

    it('should return 200 if user profile updated successfully', async () => {
        try {
            await request(app)
                .post('/api/v1/user/auth/sign-up')
                .send(userMockUp);
            const resLogIn = await request(app)
                .post('/api/v1/user/auth/login')
                .send(userMockUpLogin);
    
            const header = {'Authorization': resLogIn.data.code, 'Content-Type': 'application/json'};
            const res = await request(app)
                    .put('/api/v1/user/update')
                    .set(header)
                    .send(userUpdateMockUp);
                
            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty("status", "success");
            expect(res.body).toHaveProperty("message", "profile updated");
            expect(res.body).toHaveProperty("data");
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });

    it('should return 400 if authorization invalid', async () => {
        try {
            const header = {'Authorization': 'abcDEF123', 'Content-Type': 'application/json'};
            const res = await request(app)
                    .put('/api/v1/user/update')
                    .set(header)
                    .send(userUpdateMockUp);
                
            expect(res.statusCode).toBe(401);
            expect(res.body).toHaveProperty("status", "fail");
            expect(res.body).toHaveProperty("message", "unauthorized");
            expect(res.body).toHaveProperty("data");
            await truncate.user();
        } catch (err) {
            console.log(err.message);
            expect(err).toBe(500);
        }
    });
})