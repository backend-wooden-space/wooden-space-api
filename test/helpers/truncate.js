const { User } = require('../../models');

module.exports = {
    user: () => {
        console.log("Truncate");
        return User.destroy({ truncate: true, restartIdentity: true });
    }
};