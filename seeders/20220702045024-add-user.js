'use strict';
const bcrypt = require('bcrypt');
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const plainPassword = "celes123Password"
    const password = bcrypt.hashSync(plainPassword,bcrypt.genSaltSync(11));
    await queryInterface.bulkInsert('Users', [
     {
       username:'penjual1',
       email:'penjual1@gmail.com',
       password,
       isVerified:true,
       type:'basic',
       createdAt: new Date(),
       updatedAt: new Date()
     }
    //  ,{
    //   username:'penjual2',
    //   email:'penjual2@gmail.com',
    //   password,
    //   isVerified:true,
    //   type:'basic',
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // },{
    //   username:'pembeli1',
    //   email:'pembeli1@gmail.com',
    //   password,
    //   isVerified:true,
    //   type:'basic',
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // },{
    //   username:'pembeli2',
    //   email:'pembeli2@gmail.com',
    //   password,
    //   isVerified:true,
    //   type:'basic',
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // },{
    //   username:'pembeli3',
    //   email:'pembeli3@gmail.com',
    //   password,
    //   isVerified:true,
    //   type:'basic',
    //   createdAt: new Date(),
    //   updatedAt: new Date()
    // }
    ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await await queryInterface.bulkDelete('Users', null, {});
  }
};
