'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Product',[{
      user_id:1,
      name:"Meja Bagus",
      category_id:1,
      price:2000,
      description:"Deskripsi Meja"
    },{
      user_id:1,
      name:"Kursi Bagus",
      category_id:2,
      price:2000,
      description:"Deskripsi Kursi"
    },{
      user_id:2,
      name:"Meja Jelek",
      category_id:1,
      price:15000,
      description:"Deskripsi Meja"
    },{
      user_id:2,
      name:"Kursi Jelek",
      category_id:2,
      price:15000,
      description:"Deskripsi Kursi"
    }])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Products', null, {});
  }
};
