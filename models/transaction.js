'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Transaction.belongsTo(models.User, {foreignKey:'user_id', as:'buyer'});
      Transaction.belongsTo(models.Product, {foreignKey:'product_id', as:'product'});
    }
  }
  Transaction.init({
    user_id: DataTypes.BIGINT,
    product_id: DataTypes.BIGINT,
    price_offered: DataTypes.BIGINT,
    status: DataTypes.ENUM('offered', 'waiting', 'paid', 'send', 'finish', 'failed')
  }, {
    sequelize,
    modelName: 'Transaction',
  });
  return Transaction;
};