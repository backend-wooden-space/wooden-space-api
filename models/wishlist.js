'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Wishlist extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Wishlist.belongsTo(models.User, {foreignKey:'user_id',as:'wanter'})
      Wishlist.belongsTo(models.Product, {foreignKey:'product_id',as:'product'})
    }
  }
  Wishlist.init({
    user_id: DataTypes.BIGINT,
    product_id: DataTypes.BIGINT
  }, {
    sequelize,
    modelName: 'Wishlist',
  });
  return Wishlist;
};