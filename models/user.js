'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Product, {foreignKey: 'user_id', as: 'product'});
      User.hasOne(models.UserDetail, {foreignKey:'user_id', as:'detail'});
      User.hasMany(models.Transaction, {foreignKey: 'user_id', as: 'user_transactions'})
      User.belongsToMany(models.Product, {through: 'Wishlists', foreignKey:'user_id',as:'wanters'})
      User.hasMany(models.Notification, {foreignKey:'user_id', as:'notifications'})
    }

    checkPassword = password => {
      return bcrypt.compareSync(password, this.password);
    }

    generateToken = () => {
      const payload = {
        id: this.id,
        email:this.email
      }

      const secretKey = process.env.ACCESS_TOKEN_SECRET

      const token = jwt.sign(payload, secretKey);
      return token
    }

    static authenticate = async ({ email, password }) => {
      try {
        const user = await this.findOne({
          where: {
            email: email
          }
        });
        if (!user) return Promise.reject(new Error('user not found!'));

        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject(new Error('wrong password!'));

        return Promise.resolve(user);

      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  User.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    isVerified: DataTypes.BOOLEAN,
    type: DataTypes.ENUM('basic', 'google')
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};