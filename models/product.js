'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Product.belongsTo(models.User, {foreignKey: 'user_id', as: 'owner'});
      Product.hasMany(models.ProductGallery, {foreignKey: 'product_id', as: 'product_images'});
      Product.belongsTo(models.Category, {foreignKey:'category_id', as:'category'})
      Product.hasMany(models.Transaction, {foreignKey: 'product_id', as:'product_transactions'})
      Product.belongsToMany(models.User, {through:"Wishlists", foreignKey:'product_id',as:'product_wishlists'})
    }
  }
  Product.init({
    user_id: DataTypes.BIGINT,
    name: DataTypes.STRING,
    category_id: DataTypes.BIGINT,
    price: DataTypes.BIGINT,
    description: DataTypes.STRING,
    status: DataTypes.ENUM('available', 'sold'),
    posted: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};