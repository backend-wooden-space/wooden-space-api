require('dotenv').config();
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require("cors");

const indexRouter = require('./routes/index');

const app = express();
const allowList = ['http://localhost:3000', 'http://localhost:8080', 'https://wooden-space-authorization.herokuapp.com', 'https://accounts.google.com', 'https://woodenspace.vercel.app']
const corsOptions = {
  origin: allowList,
  credentials:true
}
app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(express.json());
// app.use("/image", express.static("public/uploads"));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/v1', indexRouter);

module.exports = app;