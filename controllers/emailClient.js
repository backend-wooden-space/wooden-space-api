const { google } = require('googleapis');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const {User} = require('../models');
const {
    GOOGLE_MAIL_CLIENT_ID,
    GOOGLE_MAIL_CLIENT_SECRET,
    GOOGLE_MAIL_SIGNIN_URI,
    GOOGLE_MAIL_REFRESH_TOKEN,
    GOOGLE_MAIL_SENDER,
    APP_MAIN_URL,
    APP_MAIN_URL_FE
} = process.env;

const oauth2EmailClient = new google.auth.OAuth2(
    GOOGLE_MAIL_CLIENT_ID,
    GOOGLE_MAIL_CLIENT_SECRET,
    GOOGLE_MAIL_SIGNIN_URI 
);

oauth2EmailClient.setCredentials({
    refresh_token:GOOGLE_MAIL_REFRESH_TOKEN
});

const sendEmail = async (to,subject,html) => {
    try {
        const accessToken = await oauth2EmailClient.getAccessToken();
        // console.log(accessToken);
        const transport = nodemailer.createTransport({
            service:'gmail',
            auth: {
                type: 'OAuth2',
                user: GOOGLE_MAIL_SENDER,
                clientId: GOOGLE_MAIL_CLIENT_ID,
                clientSecret: GOOGLE_MAIL_CLIENT_SECRET,
                refreshToken: GOOGLE_MAIL_REFRESH_TOKEN,
                accessToken
            }
        })

        const mailOptions = {
            to, subject, html
        };

        const response = await transport.sendMail(mailOptions);
        return response
    } catch (err) {
        return err
    }
}


module.exports = {
    forgotPassword: async (req,res) => {
        try {
            const email = req.body.email;
    
            const user = await User.findOne({
                where: {
                    email
                }
            })
    
            if (!user) {
                return res.status(400).json({
                    status:'fail',
                    message:'invalid user',
                    data:null
                })
            }
    
            const tokenPayload = {
                id:user.id,
                email:user.email,
                use_case:"reset password"
            }
    
            const secret = process.env.MAIL_ACCESS_TOKEN_SECRET + user.password
    
            const token = jwt.sign(tokenPayload, secret, {expiresIn: '15m'});
    
    
            const html =`<p>Hello ${user.username}, here is your link to reset your password, ${APP_MAIN_URL_FE}/reset_password/?id=${user.id}&code=${token}</p>`
    
            const response = await sendEmail(email, "Wooden Space Reset Password", html);

            console.log(response);
    
            return res.status(200).json({
                status:'success',
                message:'email sent',
                data:null
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    emailVerification: async (req,res) => {
        try {
            const {id,email} = req.user;

            const emailVerificationTokenPayload = {
                id,
                email,
                use_case:'email verification'
            };

            const emailVerificationToken = jwt.sign(emailVerificationTokenPayload, process.env.MAIL_ACCESS_TOKEN_SECRET, {expiresIn: "15m"});

            const html = `<p>Hello ${email}, here is your link to verify your email, ${APP_MAIN_URL}/api/v1/auth/email_verification?code=${emailVerificationToken}</p>`

            await sendEmail(email, "Wooden Space Email Verification", html);

            // console.log(response);
            
            return res.status(200).json({
                status:'success',
                message:'email sent',
                data:null
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}
