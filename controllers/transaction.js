const {Transaction,User,Product,ProductGallery,UserDetail,Notification} = require('../models');
const {Op} = require('sequelize');
const transactionStep = ['offered','accepted','finish'];
const buyerStatus = ['offered']
const sellerStatus = ['accepted','finish']
module.exports = {
    startTransaction: async (req,res) => {
        try {
            const user_id = req.user.id;
            const {product_id,price_offered} = req.body;
            const isUserExist = await User.findByPk(user_id, {
                include: {
                    model:UserDetail,
                    as:'detail'
                }
            });
            if (!isUserExist) {
                return res.status(401).json({
                    status:'fail',
                    message:'user doesn\'t exist',
                    data:null
                })
            }
            const isUserDetailNull = Object.values(isUserExist.detail.dataValues).some(value => value===null);
            console.log(isUserDetailNull);
            if (isUserDetailNull) {
                return res.status(401).json({
                    status:'fail',
                    message:'user need to fill all profile',
                    data:null
                })
            }

            const isProductExist = await Product.findByPk(product_id, {
                include:[{
                    model:User,
                    as:'owner',
                    attributes:['id','username']
                }]
            });
            if (!isProductExist) {
                return res.status(401).json({
                    status:'fail',
                    message:'product doesn\'t exist',
                    data:null
                })
            }

            if (isProductExist.owner.id == user_id) {
                return res.status(401).json({
                    status:'fail',
                    message:'unable to start transaction for your own product',
                    data:null
                })
            }

            const transaction = await Transaction.findOne({
                where: {
                    user_id,
                    product_id
                }
            })

            if (transaction) {
                return res.status(401).json({
                    status:'fail',
                    message:'transaction is already exist',
                    data:null
                })
            }

            if (isProductExist.status === 'sold') {
                return res.status(401).json({
                    status:'fail',
                    message:'product_sold',
                    data:null
                })
            }
            const newTransaction = await Transaction.create({
                user_id,
                product_id,
                price_offered,
                status:'offered'
            });

            const newTransactionFullData = await Transaction.findByPk(newTransaction.id, {
                include: [{
                    model:User,
                    as:'buyer',
                    attributes:['id','email'],
                    include:{
                        model:UserDetail,
                        as:'detail',
                        attributes:['name','city','address','phone_number','avatar_url']
                    }
                }, {
                    model:Product,
                    as:'product'

                }]
            })

            // console.log(newTransactionFullData);

            // const notificationTitle = 'Transaction';
            const notificationMessage = `Transaction started for product ${newTransactionFullData.product_id}, ${newTransactionFullData.product.name} with offered price ${newTransactionFullData.price_offered}`

            Notification.create({
                user_id:newTransactionFullData.user_id,
                title:'TransactionBuyer',
                message:notificationMessage,
                isRead:false
            })

            Notification.create({
                user_id:newTransactionFullData.product.user_id,
                title:'TransactionSeller',
                message:notificationMessage,
                isRead:false
            })

            return res.status(201).json({
                status:'success',
                message:'transaction started',
                data:newTransactionFullData
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    sellerUpdateTransaction: async (req,res) => {
        try {
            const {status} = req.body
            
            const transaction_id = req.params.id

            const user_id = req.user.id

            const currentTransaction = await Transaction.findByPk(transaction_id, {
                include:[{
                    model:Product,
                    as:'product'
                }]
            });

            // console.log(currentTransaction.product);

            if (!currentTransaction) {
                return res.status(401).json({
                    status:"fail",
                    message:`transaction doesn\'t exist`,
                    data:null
                })
            }

            if (user_id != currentTransaction.product.user_id) {
                return res.status(401).json({
                    status:"fail",
                    message:`unauthorized`,
                    data:null
                })
            }

            if (!sellerStatus.includes(status)) {
                return res.status(401).json({
                    status:"fail",
                    message:`unauthorized, unable to changed status to ${status}`,
                    data:null
                })
            }

            const currentStatus = currentTransaction.status;
            // console.log(currentStatus);
            if (status != transactionStep[transactionStep.indexOf(currentStatus)+1]) {
                return res.status(401).json({
                    status:"fail",
                    message:`can only be updated to ${transactionStep[transactionStep.indexOf(currentStatus)+1]}`,
                    data:null
                })
            }

            const updatedTransaction = await currentTransaction.update({
                status
            })

            let buyerNotifications = [{
                user_id:currentTransaction.user_id,
                title:'TransactionBuyer',
                message:`Transaction id ${currentTransaction.id}, for product ${currentTransaction.product.id}, ${currentTransaction.product.name} has ${status}`,
                isRead:false
            }, {
                user_id:currentTransaction.product.user_id,
                title:'TransactionSeller',
                message:`Transaction id ${currentTransaction.id}, for product ${currentTransaction.product.id}, ${currentTransaction.product.name} has ${status}`,
                isRead:false
            }]

            if (status == 'accepted') {
                await currentTransaction.product.update({
                    status:'sold'
                })

                const failedTransactions = await Transaction.findAll({
                    where:{
                        product_id:currentTransaction.product_id,
                        id:{
                            [Op.not]:currentTransaction.id
                        },
                        status: {
                            [Op.not]:'failed'
                        }
                    }
                })

                await Transaction.update({
                    status:'failed'
                },{
                    where:{
                        product_id:currentTransaction.product_id,
                        id:{
                            [Op.not]:currentTransaction.id
                        },
                        status: {
                            [Op.not]:'failed'
                        }
                    }
                })

                // console.log(failedTransactions);

                for (const key of Object.keys(failedTransactions)) {
                    console.log(failedTransactions[key]);
                    buyerNotifications.push({
                        user_id:failedTransactions[key].user_id,
                        title:'Transaction',
                        message:`Transaction id ${failedTransactions[key].id}, for product ${currentTransaction.product.id}, ${currentTransaction.product.name} has failed`,
                        isRead:false
                    })
                }
            }

            Notification.bulkCreate(buyerNotifications);

            return res.status(201).json({
                status:'success',
                message:'transaction status updated',
                data:updatedTransaction
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    cancelTransaction: async (req,res) => {
        try {
            const user_id = req.user.id;
            // console.log("masuk");
            const transaction_id = req.params.id
            const transaction = await Transaction.findByPk(transaction_id, {
                include:[{
                    model:User,
                    as:'buyer',
                    attributes:['id','username']
                },{
                    model:Product,
                    as:'product'
                }]
            })
            const currentProductStatus = transaction.product.status;
            // console.log(user_id);

            if (!(user_id == transaction.product.user_id)) {
                return res.status(401).json({
                    status:'fail',
                    message:'unauthorized user',
                    data:transaction
                })
            }

            if (transaction.status == 'finish') {
                return res.status(401).json({
                    status:'fail',
                    message:'unable to cancel',
                    data:transaction
                })
            }

            if (transaction.status == 'failed') {
                return res.status(401).json({
                    status:'fail',
                    message:'transaction has already failed',
                    data:transaction
                })
            }

            const updatedTransaction = await transaction.update({
                status:'failed'
            })

            await transaction.product.update({
                status:'available'
            })

            Notification.create({
                user_id:transaction.user_id,
                title:'TransactionBuyer',
                message:`Transaction id ${transaction.id}, for product ${transaction.product.id}, ${transaction.product.name} has failed`,
                isRead:false
            })

            Notification.create({
                user_id:transaction.product.user_id,
                title:'TransactionSeller',
                message:`Transaction id ${transaction.id}, for product ${transaction.product.id}, ${transaction.product.name} has failed`,
                isRead:false
            })

            if (currentProductStatus == 'sold') {
                let buyerNotifications = [];
                const failedTransactions = await Transaction.findAll({
                    where:{
                        status:'failed',
                        id: {[Op.not]:transaction.id},
                        product_id:transaction.product.id
                    }
                })

                for (const key of Object.keys(failedTransactions)) {
                    buyerNotifications.push({
                        user_id:failedTransactions[key].user_id,
                        title:'TransactionBuyer',
                        message:`Transaction id ${failedTransactions[key].id}, for product ${transaction.product.id}, ${transaction.product.name} can be restarted`,
                        isRead:false
                    })
                }

                Notification.bulkCreate(buyerNotifications);
            }

            return res.status(200).json({
                status:'success',
                message:'transaction canceled',
                data:updatedTransaction
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    startOverTransaction: async (req,res) => {
        try {
            const transaction_id = req.params.id
            const user_id = req.user.id
            const {price_offered} = req.body
    
            const currentTransactionState = await Transaction.findByPk(transaction_id,{
                include:[{
                    model:User,
                    as:'buyer',
                    attributes:['id','username']
                },{
                    model:Product,
                    as:'product'
                }]
            });
            
            if (user_id != currentTransactionState.buyer.id) {
                return res.status(401).json({
                    status:'fail',
                    message:'unauthorized to start over transaction',
                    data:null
                })
            }

            if (currentTransactionState.status != 'failed') {
                return res.status(401).json({
                    status:'fail',
                    message:'unable to start over transaction. transaction is on progres or already finished',
                    data:null
                })
            }

            // console.log(currentTransactionState);

            if (!currentTransactionState.product) {
                return res.status(401).json({
                    status:'fail',
                    message:'product has been deleted by the owner',
                    data:null
                })
            }

            if (currentTransactionState.product.status == 'sold') {
                return res.status(401).json({
                    status:'fail',
                    message:'product sold',
                    data:null
                })
            }

            const transaction = await currentTransactionState.update({
                price_offered,
                status:'offered'
            })

            Notification.create({
                user_id:currentTransactionState.user_id,
                title:'TransactionBuyer',
                message:`Transaction id ${currentTransactionState.id}, for product ${currentTransactionState.product.id}, ${currentTransactionState.product.name} has restarted`,
                isRead:false
            })

            Notification.create({
                user_id:currentTransactionState.product.user_id,
                title:'TransactionSeller',
                message:`Transaction id ${currentTransactionState.id}, for product ${currentTransactionState.product.id}, ${currentTransactionState.product.name} has restarted`,
                isRead:false
            })

            return res.status(201).json({
                status:'success',
                message:'transaction restart',
                data:transaction
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    buyerListTransaction: async (req,res) => {
        try {
            const user_id = req.user.id;

            let transactions = await Transaction.findAll({
                where:{
                    user_id
                },
                include:[{
                    model:Product,
                    as:'product',
                    include: [{
                        model:ProductGallery,
                        as:'product_images'
                    }, {
                        model:User,
                        as:'owner',
                        attributes:['username', 'email'],
                        include: [{
                            model:UserDetail,
                            as:'detail'
                        }]
                    }]
                }],
                order:[['updatedAt','ASC']]
            })

            transactions = transactions.filter(transaction => transaction.product != null)


            // if (!transactions.length) {
            //     return res.status(401).json({
            //         status:'fail',
            //         message:'no transaction yet',
            //         data:null
            //     })
            // } 

            return res.status(200).json({
                status:'success',
                message:'transactions retrieved',
                data:transactions
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    sellerListTransactionByProduct: async (req,res) => {
        try {
            const user_id = req.user.id;
            
            
            let rawProductTransactions = await Product.findAll({
                where: {
                    user_id
                },
                include: [{
                    model: Transaction,
                    as:'product_transactions',
                    include:[{
                        model:User,
                        as:'buyer',
                        attributes:['username','email'],
                        include: [{
                            model:UserDetail,
                            as:'detail'
                        }]
                    }]
                }, {
                    model:ProductGallery,
                    as:'product_images'
                }],
                order:[['updatedAt','ASC']]
            })
            

            rawProductTransactions = rawProductTransactions.filter(product => product.product_transactions.length > 0)

            // const rawTransactions = await Transaction.findAll({
            //     include: [{
            //         model:Product,
            //         as:'product',
            //         include: {
            //             model:User,
            //             as:'owner',
            //             attributes:['id','username','email'],
            //             where: {
            //                 id:user_id
            //             }
            //         }
            //     }]
            // })

            // const transactions = rawTransactions.filter(data => data.dataValues.product != null)
            // const productTransactions = rawProductTransactions.filter(data => data.dataValues.product_transactions.length != 0)
            // console.log(productTransactions);
            // if (!productTransactions.length) {
            //     return res.status(401).json({
            //         status:'fail',
            //         message:'no transaction yet',
            //         data:null
            //     })
            // }

            return res.status(200).json({
                status:'success',
                message:'transactions retrieved',
                data:rawProductTransactions
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    sellerReadTransaction: async (req,res) => {
        try {
            const user_id = req.user.id;

            const transaction_id = req.params.id;

            const transaction = await Transaction.findByPk(transaction_id, {
                include: [{
                    model:Product,
                    as:'product',
                    include: [{
                        model:ProductGallery,
                        as:'product_images'
                    }]
                }, {
                    model:User,
                    as:'buyer',
                    attributes:['id','email'],
                    include: [{
                        model:UserDetail,
                        as:'detail'
                    }]
                }]
            })

            if (user_id != transaction.product.user_id) {
                return res.status(400).json({
                    status:'fail',
                    message:'unauthorized',
                    data:null
                })
            }

            return res.status(200).json({
                status:'success',
                message:'transaction retrieved',
                data:transaction
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}