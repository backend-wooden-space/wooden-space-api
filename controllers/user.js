require('dotenv').config();
const {
    IMAGEKIT_PUBLIC_KEY,
    IMAGEKIT_SECRET_KEY,
    IMAGE_KIT_URL
} = process.env;

const Validator = require('fastest-validator');
const v = new Validator();
const bcrypt = require('bcrypt');
const ImageKit = require('imagekit');

const  imageKit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC_KEY,
    privateKey : IMAGEKIT_SECRET_KEY,
    urlEndpoint : IMAGE_KIT_URL
})

const {User, UserDetail} = require('../models');

module.exports = {
    updateProfile: async (req,res) => {
        try {
            const profileScheme = {
                name:{type: "string", pattern:'^[A-z\\s]+$', optional:true},
                city:{type: "string", pattern:'^[A-z\\s]+$',optional:true},
                address:{type: "string",optional:true},
                phone_number:{type: "string", pattern:'^[+]?[0-9][0-9]{5,13}$',optional:true}
            }

            const validationResult = v.validate(req.body,profileScheme);

            if (validationResult.length) {
                return res.status(401).json({
                    status:'fail',
                    message:validationResult,
                    data:null
                })
            };
            
            const userDetail = await UserDetail.findOne({
                where: {
                    user_id:req.user.id
                }
            });


            const {
                name,
                city,
                address,
                phone_number
            } = req.body;
            

            if (req.file) {
                const file = req.file.buffer.toString("base64");
                const fileName ='avatar_' + Date.now() + '_' + req.user.id;
                if (userDetail.avatar_imagekit_id != null) {
                    await imageKit.deleteFile(userDetail.avatar_imagekit_id)
                }
    
                const imagekitUploadResult = await imageKit.upload({
                    file: file,
                    fileName: fileName,
                    folder: 'userAvatar'
                });            
                
                await userDetail.update({
                    avatar_url:imagekitUploadResult.url,
                    avatar_imagekit_id:imagekitUploadResult.fileId
                });

            }

            await userDetail.update({
                name,
                city,
                address,
                phone_number
            });

            return res.status(200).json({
                status:'success',
                message:'user updated',
                data:userDetail
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }, 

    getUserProfile: async (req,res) => {
        try {
            const user_id = req.user.id;
    
            const user = await User.findOne({
                where: {
                    id:user_id
                },
                attributes: {exclude:['password','type']},
                include: {
                    model:UserDetail,
                    as:'detail',
                    attributes:{exclude:['user_id']}
                }
            })
    
            if (!user) {
                return res.status(401).json({
                    status:'fail',
                    message:'user not found'
                })
            }
    
            return res.status(200).json({
                status:'success',
                message:'user data retrieved',
                data:user
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    getUser : async (req,res) => {
        try {
            const users = await User.findAll({
                attributes:{exclude:['password','type']},
                include:{
                    model:UserDetail,
                    as:'detail'
                }
            })

            return res.status(200).json({
                status:"success",
                message:"all user data retrieved",
                data:users
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    getUserDetail: async (req,res) => {
        try {
            const user_id = req.params.id;

            const user = await User.findOne({
                where: {
                    id:user_id
                },
                attributes: {exclude:['password','type']},
                include: {
                    model:UserDetail,
                    as:'detail'
                }
            })
    
            if (!user) {
                return res.status(401).json({
                    status:'fail',
                    message:'user not found'
                })
            }
    
            return res.status(200).json({
                status:'success',
                message:'user data retrieved',
                data:user
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }, 

    updateUser: async (req,res) => {
        try {
            const user_id = req.user.id;
            // console.log(user_id);

            const userScheme = {
                username: {type:"string",optional:true},
                password: {type: "string", pattern: '^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]', min:8,optional:true}
            }

            const {username,password,oldPassword} = req.body;

            if (!(password && oldPassword)) {
                return res.status(401).json({
                    status:'fail',
                    message:'update password request should contain both password and oldpassword',
                    data:null
                })
            } 

            const validationResult = v.validate({
                username,
                password
            }, userScheme);

            if (validationResult.length) {
                return res.status(401).json({
                    status:'fail',
                    message:validationResult,
                    data:null
                })
            }

            const user = await User.findByPk(user_id);

            let passwordUpdate;

            if (password) {
                const oldPasswordData = user.password
                if (oldPasswordData) {
                    if (!oldPassword) {
                        return res.status(401).json({
                            status:'fail',
                            message:'old password needed',
                            data:null
                        })
                    }
                    const isValid = bcrypt.compareSync(oldPassword, oldPasswordData);
                    if (isValid) {
                        passwordUpdate = bcrypt.hashSync(password, bcrypt.genSaltSync(11));
                    } else {
                        return res.status(401).json({
                            status:'fail',
                            message:'old password incorrect',
                            data:null
                        })
                    }
                } else {
                    passwordUpdate = bcrypt.hashSync(password, bcrypt.genSaltSync(11));
                }
            }

            await user.update({
                username,
                password:passwordUpdate
            });

            return res.status(401).json({
                status:'success',
                message:'user updated',
                data: {
                    username:user.username
                }
            })


        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}