const {Category} = require('../models');

module.exports = {
    addCategory: async (req,res) => {
        try {
            const category = req.body.name;
            const [newCategory,isCreated] = await Category.findOrCreate({
                where: {
                    name:category
                },
                defaults: {
                    name:category
                }
            });

            if (!isCreated) {
                return res.status(401).json({
                    status:'fail',
                    message:'category is already exist',
                    data:null
                })
            }

            return res.status(200).json({
                status:'success',
                message:'new category added',
                data:newCategory
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    deleteCategory: async (req,res) => {
        try {
            const id = req.params.id;

            const category = await Category.destroy({
                where: {
                    id
                }
            })

            if(!category) {
                return res.status(401).json({
                    status:"fail",
                    message:"no data deleted",
                    data:null
                })
            }

            return res.status(201).json({
                status:'success',
                message:"deleted",
                data:null
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    getAllCategory: async (req,res) => {
        try {
            const categories = await Category.findAll();

            res.status(200).json({
                status:'success',
                message:'categories retrieved',
                data:categories
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}