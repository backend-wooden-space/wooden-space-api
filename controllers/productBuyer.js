const {User,Product,Category,ProductGallery,UserDetail,Wishlist} = require('../models');
const {Op} = require('sequelize');
require('dotenv').config();


module.exports = {
    getProduct:async(req,res)=>{
        try {
            const user_id = req.user.id;
            const category_id = req.query.category_id
            // console.log(req.query);
            const page = req.query.page ? req.query.page:1
            const keyword = req.query.keyword
            const sort = req.query.sort
            let queryWhere={
                posted:true,
                status:'available'
            };
            let queryOrder = [['price','DESC']];
            if (category_id) {
                queryWhere.category_id = category_id;
            }
            
            if (keyword) {
                queryWhere.name = {
                    [Op.iLike]:`%${keyword}%`
                }
            }
            if (sort == 'lowest_price') queryOrder= [['price','ASC']]
            else queryOrder= [['price','DESC']]
            const products = await Product.findAll({
                include: [{
                    model:ProductGallery,
                    as:'product_images',
                    order:[['id','ASC'],['url','NULLS LAST']]
                }, {
                    model:Category,
                    as:'category'
                }, {
                    model:User,
                    as:'owner',
                    attributes:['id','username', 'email'],
                    where: {
                        id: {
                            [Op.not]: user_id
                        }
                    }
                }],
                where:queryWhere,
                offset:12 * (page-1),
                limit:12,
                order:queryOrder
            });

            return res.status(201).json({
                status:'success',
                message:'products retreived',
                data:products
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    getProductDetail: async(req,res)=>{
        try {
            const user_id = req.user.id;
            const product_id = req.params.id;

            const product = await Product.findByPk(product_id, {
                include: [{
                    model:ProductGallery,
                    as:'product_images',
                    order:[['id','ASC'],['url','NULLS LAST']]
                }, {
                    model:Category,
                    as:'category'
                }, {
                    model:User,
                    as:'owner',
                    attributes:['id','username', 'email'],
                    include: [{
                        model:UserDetail,
                        as:'detail'
                    }]
                }]
            });

            const isWished = await Wishlist.findOne({
                where: {
                    user_id,
                    product_id
                }
            });

            if (isWished) {
                product.setDataValue('wished',true);
            } else {
                product.setDataValue('wished',false);
            }

            if (!product) {
                return res.status(401).json({
                    status:'fail',
                    message:'product doesn\'t exist',
                    data:null
                })
            }

            if (product.posted == false) {
                return res.status(401).json({
                    status:'fail',
                    message:'product has not been posted',
                    data:null
                })
            }
            

            if (product.status == 'sold') {
                return res.status(401).json({
                    status:'fail',
                    message:'product has already sold',
                    data:product
                })
            }
            return res.status(200).json({
                status:'success',
                message:'product retrieved',
                data:product
            })
            
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}
