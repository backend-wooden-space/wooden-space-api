const { Wishlist, Product, User, UserDetail, ProductGallery , Category} = require("../models");

// create wishlist
createWishlist = async (req, res) => {
  try {
    let productId = req.params.productId;
    if (!productId || !req.user.id) {
      return res.status(400).json({
        status: "fail",
        message: "Please fill all the fields",
        data: null,
      });
    }

    let userExists = await User.findOne({
      where: { id: req.user.id },
    });


    let productExists = await Product.findOne({
      where: { id: productId },
    });

    let wishlistExists = await Wishlist.findOne({
      where: { product_id: productId, user_id: req.user.id },
    });

    let productHasUser = await Product.findOne({
      where: {
        id:productId,
        user_id: req.user.id
      }
    });

    if (!userExists) {
      return res.status(404).json({
        status: "fail",
        message: "User not found",
        data: null,
      });
    }

    if (!productExists) {
      return res.status(404).json({
        status: "fail",
        message: "Product not found",
        data: null,
      });
    }

    if (productHasUser) {
      return res.status(404).json({
        status: "fail",
        message: "Product is owned by user",
        data: null,
      });
    }

    if (productExists.status == 'sold') {
      return res.status(401).json({
        status: "fail",
        message: "Product is owned by user",
        data: null,
      })
    }

    if (!wishlistExists) {
      let newWishlist = await Wishlist.create({
        product_id: productId,
        user_id: req.user.id,
      });

      return res.status(201).json({
        status: "success",
        message: "Wishlist created successfully",
        data: newWishlist,
      });
    }

    return res.status(404).json({
      status: "fail",
      message: "Product already exist on table Wishlist",
      data: null,
    });
  } catch (err) {
    return res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

// findByUserId
getWishlists = async (req, res) => {
  try {
    let userExists = await User.findOne({
      where: { id: req.user.id },
    });

    if (!userExists) {
      return res.status(404).json({
        status: "fail",
        message: "User not found",
      });
    }

    let wishlist = await Wishlist.findAll({
      where: { user_id: req.user.id },
      include:[{
        model:Product,
        as:'product',
        include:[{
          model:User,
          as:'owner',
          attributes:['id','email'],
          include:[{
            model:UserDetail,
            as:'detail',
            attributes:['name','city','address','phone_number','avatar_url']
          }]
        }, {
          model:ProductGallery,
          as:'product_images'
        }, {
          model:Category,
          as:'category'
        }]
      }]
    });

    return res.status(200).json({
      status: "success",
      message: "Wishlist retrieved successfully",
      data: wishlist,
    });
  } catch (err) {
    return res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};


// delete wishlists by id wishlist
deleteWishlistsById = async (req, res) => {
  try {
    let { wishlistId } = req.params;

    let wishlist = await Wishlist.findOne({
      where: { id: wishlistId, user_id: req.user.id },
    });

    if (!wishlist) {
      return res.status(404).json({
        status: "fail",
        message: "Wishlist not found",
      });
    }

    let deletedWishlists = await wishlist.destroy({
      where: { id: wishlistId, user_id: req.user.id },
    });

    let allWishlist = await Wishlist.findAll({
      where: { user_id: req.user.id },
    });

    return res.status(200).json({
      status: "success",
      message: "Wishlist deleted successfully",
      data: allWishlist,
    });
  } catch (err) {
    return res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

// delete wishlist by user_id && product_id
deleteWishlistsByUserIdProductId = async (req, res) => {
  try {
    let { productId } = req.params;

    let wishlist = await Wishlist.findOne({
      where: { product_id: productId, user_id: req.user.id },
    });

    if (!wishlist) {
      return res.status(404).json({
        status: "fail",
        message: "Wishlist not found",
      });
    }

    let deletedWishlists = await wishlist.destroy({
      where: { product_id: productId, user_id: req.user.id },
    });

    let allWishlist = await Wishlist.findAll({
      where: { user_id: req.user.id },
    });

    return res.status(200).json({
      status: "success",
      message: "Wishlist deleted successfully",
      data: allWishlist,
    });
  } catch (err) {
    return res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

module.exports = {
  createWishlist,
  getWishlists,
  deleteWishlistsById,
  deleteWishlistsByUserIdProductId,
};
