const {User,Product, Category,ProductGallery,UserDetail,Transaction} = require('../models');
// const {Op} = require('sequelize');
const Validator = require('fastest-validator');
const v = new Validator();
const ImageKit = require('imagekit');
require('dotenv').config();
const transactionStep = ['offered','accepted','finish'];
const {
    IMAGEKIT_PUBLIC_KEY,
    IMAGEKIT_SECRET_KEY,
    IMAGE_KIT_URL
} = process.env;
const  imageKit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC_KEY,
    privateKey : IMAGEKIT_SECRET_KEY,
    urlEndpoint : IMAGE_KIT_URL
})
module.exports = {
    addProduct: async (req,res) => {
        try {
            const user_id = req.user.id;

            //can't post productif user detail not fully filled
            const userDetail = await UserDetail.findOne({
                where: {
                    user_id
                }
            })

            const isUserDetailNull = Object.values(userDetail.dataValues).some(value => value===null)

            if (isUserDetailNull) {
                return res.status(401).json({
                    status:'fail',
                    message:'user need to fill all profile',
                    data:null
                })
            }

            const productExist = await Product.findAll({
                where: {
                    user_id,
                    status:'available'
                }
            });

            const productCount = productExist.length

            if (productCount >= 4) {
                return res.status(401).json({
                    status:"fail",
                    message:"too much product",
                    data:null
                })
            }

            const productScheme = {
                name:{type: "string"},
                category_id:{type: "number", positive: true, integer: true},
                price:{type: "number", positive: true, integer: true},
                description:{type: "string"}
            }
            // console.log(user_id);
            let {name, category_id, price, description} = req.body
            let bodyParsed = {
                name,
                category_id:parseInt(category_id),
                price:parseInt(price),
                description
            }
            const validationMessagee = v.validate(bodyParsed, productScheme)

            if (validationMessagee.length) {
                return res.status(401).json({
                    status:"fail",
                    message:validationMessagee,
                    data:null
                })
            }
            if (!req.files) {
                return res.status(401).json({
                    status:"fail",
                    message:'product image required',
                    data:null
                })
            }

            const [newProduct, isCreated] = await Product.findOrCreate({
                where: {
                    name
                },
                defaults: {
                    user_id,
                    category_id:bodyParsed.category_id,
                    price:bodyParsed.price,
                    description,
                    status:'available',
                    posted:true
                }
            })

            if (!isCreated) {
                return res.status(401).json({
                    status:"fail",
                    message:"product with the same name is already exist",
                    data:null
                })
            }

            const productImages = req.files;
            // console.log(newProduct.dataValues.id);
            let fileCount = 0;
            for (const key of Object.keys(productImages)) {
                const image = productImages[key]
                const file = image.buffer.toString("base64");
                const fileName ='product_'+ newProduct.dataValues.id;
    
                const imagekitUploadResult = await imageKit.upload({
                    file: file,
                    fileName: fileName,
                    folder: 'productGalleries'
                });

                await ProductGallery.create({
                    product_id:newProduct.dataValues.id,
                    url:imagekitUploadResult.url,
                    file_id:imagekitUploadResult.fileId
                })
                fileCount++;
            }
            
            for (fileCount; fileCount < 4; fileCount++) {
                await ProductGallery.create({
                    product_id:newProduct.dataValues.id,
                    url:null,
                    file_id:null
                })
            }
            
            return res.status(200).json({
                status:'success',
                message:'product added',
                data:newProduct
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    updateProduct: async (req,res) => {
        try {
            const product = req.product;

            const productScheme = {
                name:{type: "string",optional:true},
                category_id:{type: "number", positive: true, integer: true,optional:true},
                price:{type: "number", positive: true, integer: true,optional:true},
                description:{type: "string",optional:true},
                posted: {type: "boolean",optional:true}
            }
            const validationMessagee = v.validate(req.body, productScheme)
            const {name, category_id, price, description,posted} = req.body   
            if (validationMessagee.length) {
                return res.status(401).json({
                    status:"fail",
                    message:validationMessagee,
                    data:null
                })
            }

            //can't update product if it has already posted
            // if (name || category_id || price ||description) {
            //     if (product.posted) {
            //         return res.status(401).json({
            //             status:'fail',
            //             message:'product has already posted',
            //             data:null
            //         })
            //     }
            // }

            // can't unpost product if it's on transaction
            const productTransactions = product.product_transactions;
            for (const key of productTransactions) {
                const transaction = productTransactions[key];
                if (transactionStep.includes(transaction.status)) {
                    return res.status(401).json({
                        status:'fail',
                        message:'product has been recorded to ongoing or finished transaction',
                        data:null
                    })
                }
            }

            if (category_id) {
                const isCategoryExist = await Category.findByPk(category_id);
    
                if (!isCategoryExist) {
                    return res.status(401).json({
                        status:'fail',
                        message:'category doesn\'t exist',
                        data:null
                    })
                }
            }

            await product.update({
                name,
                category_id,
                price,
                description,
                posted
            })

            res.status(200).json({
                status:'success',
                message:'product updated',
                data:product
            });
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    deleteProduct: async (req,res) => {
        try {
            const product = req.product;

            // if (product.posted) {
            //     return res.status(401).json({
            //         status:'fail',
            //         message:'can\'t delete posted product',
            //         data:null
            //     })
            // }

            const productTransactions = product.product_transactions;
            console.log(productTransactions);
            if (productTransactions.length) {
                for (const key of Object.keys(productTransactions)) {
                    const status = productTransactions[key].status
                    if (transactionStep.includes(status)) {
                        return res.status(401).json({
                            status:'fail',
                            message:'product has been recorded in transaction',
                            data:null
                        })
                    }
                }
            }

            const productImages = product.product_images;
            
            for (const key of Object.keys(productImages)) {
                console.log(productImages[key].file_id);
                if (productImages[key].file_id != null) await imageKit.deleteFile(productImages[key].file_id)
            }

            await Product.destroy({
                where:{
                    id:product.id
                },
                include: {
                    model:Transaction,
                    as:'product_transaction'
                }
            });

            await ProductGallery.destroy({
                where:{
                    product_id:product.id
                }
            });

            return res.status(200).json({
                status:"success",
                message:"product deleted",
                data:null
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    getProduct: async (req,res) => {
        try {
            const user_id = req.user.id;
            
            const products = await Product.findAll({
                where: {
                    user_id
                },
                include: [{
                    model: User,
                    as:'owner',
                    attributes: ['id','email'],
                    include:[{
                        model: UserDetail,
                        as:'detail',
                        attributes: ['name','city','address','phone_number','avatar_url']
                    }]
                }, {
                    model:Category,
                    as:'category',
                    attributes:['name']
                }, {
                    model:ProductGallery,
                    as:'product_images',
                    attributes:['url'],
                    order:[['id','ASC'],['url','NULL LAST']]
                }]
            })

            return res.status(201).json({
                status:'success',
                message:'products retreived',
                data:products
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    getProductDetail: async (req,res) => {
        try {
            let product = req.product;
            return res.status(200).json({
                status:'success',
                message:'product retreived',
                data:product
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}

