const {Notification} = require('../models');

module.exports = {
    getNotification: async (req,res) => {
        try {
            const user_id = req.user.id;

            const notifications = await Notification.findAll({
                where: {
                    user_id
                }
            })

            return res.status(200).json({
                status:'success',
                message:'notifications retrieved',
                data:notifications
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    },

    updateNotification :async (req,res) => {
        try {
            const user_id = req.user.id;

            const notif_id = req.params.id;

            const {title,message,isRead} = req.body

            const notification = await Notification.findOne({
                where: {
                    id:notif_id,
                    user_id
                }
            })

            if (!notification) {
                return res.status(200).json({
                    status:'fail',
                    message:'unauthorized',
                    data:null
                })
            }

            await notification.update({
                title,message,isRead
            })
            return res.status(200).json({
                status:'success',
                message:'notifications updated',
                data:notification
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}