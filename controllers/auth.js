const { User,UserDetail } = require('../models');
const bcrypt = require('bcrypt');
const Validator = require('fastest-validator');
const v = new Validator();
const {Op} = require('sequelize');
const jwt = require('jsonwebtoken');
const {
    APP_MAIN_URL_FE
} = process.env
module.exports = {
    resetPassword: async (req,res) => {
        try {
            const code = req.query.code;
    
            if (!code) {
                return res.status(401).json({
                    status:"fail",
                    message:"code unknown",
                    data: null
                })
            }
    
            const user_id = req.query.id;
    
            const user = await User.findOne({
                where: {
                    id:user_id
                }
            });
    
            const secret = process.env.MAIL_ACCESS_TOKEN_SECRET + user.password;
    
            const data = jwt.verify(code,secret);

            if (data.use_case != 'reset password') {
                return res.status(400).json({
                    status:'fail',
                    message:'invalid token usability',
                    data:null
                })
            }
    
            const profileScheme = {
                newPassword: {type: "string", pattern: '^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]', min:8},
                confirmPassword: {type: "string", pattern: '^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]', min:8}
            }
    
            const validationResult = v.validate(req.body,profileScheme);
    
            if (validationResult.length) {
                return res.status(400).json({
                    status:'fail',
                    message:validationResult,
                    data:null
                })
            };
    
            const {newPassword,confirmPassword} = req.body;
    
            if (newPassword != confirmPassword) {
                return res.status(400).json({
                    status:'fail',
                    message:'confirmation password need to be the same as new password',
                    data:null
                })
            }
    
            const hashedPassword = await bcrypt.hash(newPassword, bcrypt.genSaltSync(11));
    
            await user.update({
                password:hashedPassword
            })
    
            return res.status(200).json({
                status:"success",
                message:"password updated",
                data: {
                    username:user.username
                }
            })
        } catch (err) {
            res.status(500).json({
                status: 'error',
                message: err.message,
                data: null
            });
        }
    },

    emailVerification: async (req,res) => {
        try {
            const code = req.query.code;
    
            if (!code) {
                return res.status(400).json({
                    status:"fail",
                    message:"invalid token",
                    data:null
                })
            }
    
            const data = jwt.verify(code, process.env.MAIL_ACCESS_TOKEN_SECRET);
    
            if (data.use_case != 'email verification') {
                return res.status(400).json({
                    status:"fail",
                    message:"invalid token usability",
                    data:null
                })
            }
    
            const user = await User.findOne({
                where: {
                    id:data.id
                }
            });
    
            if(!user) {
                return res.status(400).json({
                    status:"fail",
                    message:"invalid token",
                    data:null
                })
            }
    
            if (data.email != user.email) {
                return res.status(400).json({
                    status:"fail",
                    message:"invalid token",
                    data:null
                })
            }
    
            await user.update({
                isVerified:true
            });
            res.setHeader("Content-Type", "text/html")
            return res.send(`
            <p>Akun anda sudah terverifikasi, click <a href="${APP_MAIN_URL_FE}">disini</a> untuk lanjut</p>
            `)
        } catch (err) {
            res.status(500).json({
                status: 'error',
                message: err.message,
                data: null
            });
        }
    }
};