const {Product, ProductGallery, User} = require('../models');
const ImageKit = require('imagekit');
require('dotenv').config();
const {
    IMAGEKIT_PUBLIC_KEY,
    IMAGEKIT_SECRET_KEY,
    IMAGE_KIT_URL
} = process.env;
const  imageKit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC_KEY,
    privateKey : IMAGEKIT_SECRET_KEY,
    urlEndpoint : IMAGE_KIT_URL
})
module.exports = {
    updateImage: async (req,res) => {
        try {
            const productImage = req.productImage;
    
            if (!req.file) {
                return res.status(401).json({
                    status:'fail',
                    message:'image required',
                    data:null
                })
            }
            const file = req.file.buffer.toString("base64");
            const fileName ='product_'+ productImage.product.id;
    
            if (productImage.url != null) {
                await imageKit.deleteFile(productImage.file_id)
            }
        
            const imagekitUploadResult = await imageKit.upload({
                file: file,
                fileName: fileName,
                folder: 'productGalleries'
            });
    
            await productImage.update({
                url:imagekitUploadResult.url,
                file_id:imagekitUploadResult.fileId
            })
    
            return res.status(401).json({
                status:"success",
                message:"product image updated",
                data:null
            })
        } catch (err) {
            return res.status(500).json({
                status:'error',
                message:err.message,
                data:null
            })
        }
    }
}